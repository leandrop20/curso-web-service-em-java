package br.com.devmedia.webservices.service.ws;

import java.util.List;

import javax.ejb.EJB;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import br.com.devmedia.webservices.entities.Produto;
import br.com.devmedia.webservices.service.ProdutoService;

@Path("/produtoService")
public class ProdutoWebService {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;
    @EJB
    private ProdutoService produtoService;

    public ProdutoWebService() {
        
    }

    @GET
    @Produces("application/json")
    @Path("/{categoria}/{descricao}")
    public List<Produto> getProdutoByDescricao(
		@PathParam("categoria") String cate,
		@NotNull @PathParam("descricao") String desc) {
    	if (cate != null && cate.equalsIgnoreCase("todas")) {
    		return produtoService.getProdutoByCategoriaDescricao(cate, desc);
    	} else {
    		return produtoService.getProdutoByNome(desc);
    	}
    }

}