package br.com.devmedia.webservices.control;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;

import br.com.devmedia.webservices.entities.Categoria;
import br.com.devmedia.webservices.service.CategoriaService;

@ManagedBean
@SessionScoped
public class ProdutoControl implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private CategoriaService categoriaService;
	
	public List<Categoria> getCategorias() {
		return categoriaService.getCategorias();
	}
	
}